package net.kyem.reservations;

import static org.junit.Assert.*;

import org.junit.Test;

public class DatabaseTest {

	@Test
	public void testDatabase() {
		Database testDB = new Database();
		assertEquals(0, testDB.getSeats().size());
		assertEquals(0, testDB.getFlights().size());
		assertEquals(0, testDB.getPassengers().size());
		assertEquals(0, testDB.getTickets().size());
	}
	
	@Test
	public void testAddSeat() { 
		Database testDB2 = new Database(); 
		testDB2.addSeat(2);
		assertEquals(1, testDB2.getSeats().size());
		assertEquals(2, testDB2.getSeats().get(0).getSeatNumber());
	}
	
	@Test
	public void testAddFlights() { 
		Database testDB3 = new Database(); 
		testDB3.addFlight(2, "London", "Dublin");
		assertEquals(1, testDB3.getFlights().size());
		assertEquals("London", testDB3.getFlights().get(0).getDepartureCity());
	}
	
	@Test
	public void testAddPassengers() { 
		Database testDB4 = new Database(); 
		testDB4.addPassenger("John Doe");
		assertEquals(1, testDB4.getPassengers().size());
		assertEquals("John Doe", testDB4.getPassengers().get(0).getName());
	}
	@Test 
	public void testAddNewPassenger() {
		Database testDB6 = new Database();
		// Add new passenger
		assertEquals(false, testDB6.addPassenger("Mike Kelly"));
		// Add same passenger, that should return true === user already exist
		assertEquals(true, testDB6.addPassenger("Mike Kelly"));
	}
	@Test 
	public void testBootstrap() {
		
		Database testDB7 = new Database();
		testDB7.bootstrap();
		assertEquals(6, testDB7.getSeats().size());
		assertEquals(4, testDB7.getFlights().size());

	}

}
