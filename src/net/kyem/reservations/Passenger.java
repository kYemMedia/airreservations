package net.kyem.reservations;

public class Passenger {
	
	// fields
	private String name;
	
	public Passenger() {
		name = "Unknown name";
	}
	
	public Passenger(String name) {
		setName(name);
	}
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "Passenger: " + this.getName();
	}
}
