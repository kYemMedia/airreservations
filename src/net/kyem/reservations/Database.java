package net.kyem.reservations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database {

	private ArrayList<Seat> seats;
	private ArrayList<Flight> flights;
	private ArrayList<Passenger> passengers;
	private ArrayList<Ticket> tickets;
	
	private static Logger databaseLog = Logger.getLogger(Database.class.getName());
	private static ConsoleHandler logScreen = new ConsoleHandler();

	public Database( ) {
		seats = new ArrayList<Seat>();
		flights = new ArrayList<Flight>();
		passengers = new ArrayList<Passenger>();
		tickets = new ArrayList<Ticket>();
	}

	public ArrayList<Seat> getSeats() {
		return seats;
	}

	public ArrayList<Flight> getFlights() {
		return flights;
	}

	public ArrayList<Passenger> getPassengers() {
		return passengers;
	}

	public ArrayList<Ticket> getTickets() {
		return tickets;
	}
	
	public void setLogging()	 {
		databaseLog.addHandler(logScreen);
		databaseLog.setLevel(Level.OFF);
		logScreen.setLevel(Level.OFF);
		databaseLog.setUseParentHandlers(false);
	}
	
	public void addSeat(int seatNumber) {
		 
		seats.add(new Seat(seatNumber));
	}

	public void addFlight(int flightNumber, String departureCity, String arrivalCity) {
		// 
		flights.add(new Flight(flightNumber, departureCity, arrivalCity ));
	}

	public boolean addPassenger(String passengerName) {
		 
		boolean passengerExists = false;
		// Check if the passenger exists
		for (Passenger passenger : passengers) {
			if (passengerName.equals(passenger.getName())) {
				passengerExists = true;
			}
		}	
		if (!passengerExists) {
			passengers.add(new Passenger(passengerName));
		}
		return passengerExists;
	}
	
	public String addTicket(LocalDate departureDate, String passengerName, int flightNumber, int seatNumber) {
		// Passenger
		databaseLog.fine("Beginning ticket creation");
		Passenger ticketPassenger = null;
		databaseLog.fine("Finding passenger");
		for (Passenger passenger : getPassengers()) {
			
			if (passenger.getName().equals(passengerName)) {
				ticketPassenger = passenger;
			}
		}
		
		Flight ticketFlight = null;
		databaseLog.fine("Finding flights");
		for (Flight flight : getFlights()) {
			databaseLog.finer("Comparing " + flightNumber + " to flight " + flight.getFlightNumber());
			if (flight.getFlightNumber() == flightNumber) {
				ticketFlight = flight;
			}
		}
		
		Seat ticketSeat = null;
		for (Seat seat : getSeats()) {
			if (seat.getSeatNumber() == seatNumber) {
				ticketSeat = seat;
			}
		}
		// Create Ticket
		Ticket tempTicket = new Ticket();
		tempTicket.setDepartureDate(departureDate);
		tempTicket.setPassenger(ticketPassenger);
		tempTicket.setFlight(ticketFlight);
		tempTicket.setSeat(ticketSeat);
		tickets.add(tempTicket);
		databaseLog.info("Ticket Created");
		return tempTicket.toString();
		
	}
	
	public ArrayList<Seat> getOpenSeats(LocalDate departureDate, int flightNumber) {
		ArrayList<Seat>	openSeats = getSeats();
		for ( Ticket item : getTickets()) {
			if (departureDate.equals(item.getDepartureDate()) && flightNumber == item.getFlight().getFlightNumber()) {
				openSeats.remove(item.getSeat());
			}
		}
		return openSeats;
	}
	
	/*
	 * Bootstrapping
	 */
	public void bootstrap() {
		addSeat(1);
		addSeat(2);
		addSeat(3);
		addSeat(4);
		addSeat(5);
		addSeat(6);
		
		addFlight(1000, "Los Angeles", "Chicago");
		addFlight(1010, "Chicago", "New York");
		addFlight(2000, "New York", "Chicago");
		addFlight(2010, "Chicago", "Los Angeles");
	}
	
	public void bootstrapCSV(String directory) {
		try {
			BufferedReader flightImport = new BufferedReader(new FileReader(directory));
			String flightLine;
			while ((flightLine = flightImport.readLine()) != null) {
				System.out.println(flightLine);
			}
			flightImport.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void exportSeats() {
		BufferedWriter seatExport = null;
		try {
			seatExport = new BufferedWriter(new FileWriter("/home/kyem/workspace/Reservations/export/seats.csv"));
			for (Seat item : getSeats()) {
				seatExport.write(item.toString() + "\n");
			}
			seatExport.close();
			System.out.println("Exported Seats succesfully");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
