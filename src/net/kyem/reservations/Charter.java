package net.kyem.reservations;

import java.time.LocalDateTime;

public class Charter extends Flight {
	// fields
	private LocalDateTime departureDateTime;
	
	public Charter(int flightNumber, String departureCity, String arrivalCity, String departureDateTime) {
		super(flightNumber, departureCity, arrivalCity);
		setDepartureDateTime(departureDateTime);
	}

	/**
	 * @return the departureDateTime
	 */
	public LocalDateTime getDepartureDateTime() {
		return departureDateTime;
	}

	/**
	 * @param departureDateTime the departureDateTime to set
	 */
	public void setDepartureDateTime(String departureDateTime) {
		
		this.departureDateTime = LocalDateTime.parse(departureDateTime);
	}
	
	
	
	
	
	
	
	
	
	
	
}
