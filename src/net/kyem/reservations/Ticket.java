package net.kyem.reservations;

import java.time.LocalDate;

public class Ticket {
	
	// field
	private LocalDate departureDate;
	private Passenger passenger;
	private Flight flight;
	private Seat seat;
	
	public Ticket() {
		departureDate = LocalDate.now();
	}
	
	/**
	 * 
	 */
	public Ticket(Passenger passenger,Flight flight, Seat seat) {
		this.passenger = passenger;
		this.flight = flight;
		this.seat = seat;
		departureDate = LocalDate.now();
	}
	
	public LocalDate getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDate departureDate) {
		this.departureDate = departureDate;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public Seat getSeat() {
		return seat;
	}

	public void setSeat(Seat seat) {
		this.seat = seat;
	}
	
	public String toString() {
		return "Ticket: " + this.getPassenger().getName() + " departing on flight "
						  + this.getFlight().getFlightNumber() + " from " 
						  + this.getFlight().getDepartureCity() + " to " 
						  + this.getFlight().getArrivalCity() + " on "
						  + this.departureDate + " in seat " + this.getSeat().getSeatNumber();
	}
}
