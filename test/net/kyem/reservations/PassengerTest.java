package net.kyem.reservations;

import static org.junit.Assert.*;

import org.junit.Test;

public class PassengerTest {

	@Test
	public void testPassenger() {
		Passenger testPassenger1 = new Passenger();
		String result = testPassenger1.getName();
		assertEquals("Unknown name", result);
		
	}
	
	@Test
	public void testPassengerConstructor() {
		Passenger testPassenger2 = new Passenger("John Doe");
		assertEquals("John Doe", testPassenger2.getName());
		
	}

	@Test
	public void testSetName() {
		Passenger testPassenger3 = new Passenger();
		testPassenger3.setName("John Doe");
		assertEquals("John Doe", testPassenger3.getName());
	}
	
	@Test
	public void testToString() {
		Passenger testPassenger4 = new Passenger();
		testPassenger4.setName("John Doe");
		assertEquals("Passenger: John Doe", testPassenger4.toString());
	}
}
