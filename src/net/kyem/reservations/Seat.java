package net.kyem.reservations;

public class Seat {
	
	private int seatNumber;
	
	/**
	 * Constructor
	 */
	public Seat () {
		this.seatNumber = 10;
	}
	
	public Seat (int seatNumber) {
		this.seatNumber = seatNumber;
	}
	/**
	 * @return the seat number of the passenger
	 */
	public int getSeatNumber() {
		return seatNumber;
	}

	/**
	 * @param seat the seat to set
	 */
	public void setSeatNumber(int seat) {
		this.seatNumber = seat;
	}
	
	public String toString() {
		return "Seat: " + this.getSeatNumber();
	}
}
