package net.kyem.reservations;

import static org.junit.Assert.*;

import org.junit.Test;

public class TicketTest {
	
	@Test
	public void testTicketConstructor() {		
		Passenger passenger1 = new Passenger("John Doe");
		Flight flightTest1 = new Flight(1550, "Dublin", "Luton");
		Seat seatTest1 = new Seat(1);
		Ticket testTicket4 = new Ticket(passenger1, flightTest1, seatTest1);
		assertEquals("Ticket: John Doe departing on flight 1550 from Dublin to Luton on " + testTicket4.getDepartureDate() + " in seat 1", testTicket4.toString());
	}

	@Test
	public void testTicketPassenger() {
		Ticket testTicket1 = new Ticket();
		Passenger passenger1 = new Passenger("John Doe");
		testTicket1.setPassenger(passenger1);
		assertEquals("John Doe", testTicket1.getPassenger().getName());
	}
	@Test
	public void testTicketFlight() {
		Ticket testTicket2 = new Ticket();
		Flight flightTest1 = new Flight(300, "Dublin", "Luton");
		testTicket2.setFlight(flightTest1);
		assertEquals("Luton", testTicket2.getFlight().getArrivalCity());
	}
	@Test
	public void testTicketSeat() {
		Ticket testTicket3 = new Ticket();
		Seat seatTest1 = new Seat();
		seatTest1.setSeatNumber(2);
		testTicket3.setSeat(seatTest1);
		assertEquals(2, testTicket3.getSeat().getSeatNumber());
	}

}
