package net.kyem.reservations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Console {

	public static void main(String[] args) {
		// Init DB
		Database prodDB = new Database();
		prodDB.bootstrap();
		prodDB.setLogging();
		
		// Init console
		String passengerName = null;
		boolean always = true;
//		boolean validSeat = false;
		int flightNumber = 0;
		int seatNumber = 0;
		LocalDate departureDate = LocalDate.now();
		
		BufferedReader screenInput = new BufferedReader(new InputStreamReader(System.in));
		while (always) {
			
			System.out.println("Enter passenger name: ");
				try {
					passengerName = screenInput.readLine();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Unknow characters");
					
				}
			
			if (prodDB.addPassenger(passengerName)) {
				System.out.println("Welcome back " + passengerName);
			} else {
				System.out.println("Welcome " + passengerName);
			}
//			always = false;
//			show flights and ask for flight
			System.out.println("\nHere are the available flights:");
			for (Flight flight : prodDB.getFlights()) {
				System.out.println(flight);
			}
			System.out.println("Enter the flight number to book: ");
			try {
				flightNumber = Integer.parseInt(screenInput.readLine());
			} catch (NumberFormatException e) {
				System.out.println("Please enter a number");
				Logger.getGlobal().warning("Flight must be an integer");
			} catch (Exception e) {
//				TODO from available flight numbers
				System.out.println("Please enter a flight number");
			} 
			
//			show available seats and ask
			System.out.println("\nAssuming you are flying today,");
			System.out.println("\nHere are the available seats on that flight:");
			// TODO get date from user input
			ArrayList<Seat>	openSeats = prodDB.getOpenSeats(departureDate, flightNumber);
			
			for (Seat seat : openSeats) {
				System.out.println(seat);
			}
			System.out.println("Enter the seat number to book: ");
			try {
				// TODO Only from available seat number
				seatNumber = Integer.parseInt(screenInput.readLine());
			} catch (NumberFormatException e) {
				System.out.println("Please enter a number");
			} catch (Exception e) {
				System.out.println("Please enter a seat number");
			} 
//			create ticket and return info
			
			String ticketInfo = prodDB.addTicket(departureDate, passengerName, flightNumber, seatNumber);
			System.out.println("\nReservation have been successful. Here are your details:");
			System.out.println(ticketInfo +"\n");
			
			
			
			
		}
		
	}

}
