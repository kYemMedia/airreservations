package net.kyem.reservations;

import static org.junit.Assert.*;

import org.junit.Test;

public class SeatTest {

	@Test
	public void testSeat() {
		Seat testSeat1 = new Seat();
		assertEquals(10, testSeat1.getSeatNumber());
		
	}
	
	public void testSeatConstructor() {
		Seat testSeat3 = new Seat(5);
		assertEquals(5, testSeat3.getSeatNumber());
		
	} 
	
	@Test
	public void testSetSeat() {
		Seat testSeat2 = new Seat();
		testSeat2.setSeatNumber(5);
		assertEquals(5, testSeat2.getSeatNumber());
		
	}
	
	public void testSeatToString() {
		Seat testSeat4 = new Seat(5);
		assertEquals("Seat: 5", testSeat4.toString());
		
	} 
}
