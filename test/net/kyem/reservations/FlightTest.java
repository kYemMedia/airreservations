package net.kyem.reservations;

import junit.framework.TestCase;

import org.junit.Test;

public class FlightTest extends TestCase {

	@Test
	public void testFlight() {
		Flight testFlight1 = new Flight();
		assertEquals("Unknown Departure City", testFlight1.getDepartureCity());
		assertEquals("Unknown Arrival City", testFlight1.getArrivalCity());
		assertEquals(100, testFlight1.getFlightNumber());
	}
	
	@Test
	public void testFlightConstructor() {
		Flight testFlight5 = new Flight(10, "Los Angeles", "Chicago");
		assertEquals("Los Angeles", testFlight5.getDepartureCity());
		assertEquals("Chicago", testFlight5.getArrivalCity());
		assertEquals(10, testFlight5.getFlightNumber());
	}
	
	@Test
	public void testSetDepartureCity() {
		Flight testFlight2 = new Flight();
		testFlight2.setDepartureCity("London");
		assertEquals("London", testFlight2.getDepartureCity());
	}
	@Test
	public void testSetArrivalCity() {
		Flight testFlight3 = new Flight();
		testFlight3.setArrivalCity("Manchester");
		assertEquals("Manchester", testFlight3.getArrivalCity());
	}
	@Test
	public void testSetFlightNumber() {
		Flight testFlight4 = new Flight();
		testFlight4.setFlightNumber(44);
		assertEquals(44, testFlight4.getFlightNumber());
	}
	
	@Test
	public void testToString() {
		Flight testFlight6 = new Flight(10, "Los Angeles", "Chicago");
		assertEquals("Flight: 10 Los Angeles - Chicago", testFlight6.toString());
	}
}
